# MConverter
Simple app to convert values

## Instructions to run

### Install play framework
[Download and instructions](https://www.playframework.com/download)

### Use the command line
Run the project from its directory:

*The first time this takes a while, is when the Apache Ivy (via sbt) implement managed dependencies*
```
activator run
```

**After run the project, you'll need to click on "Apply" to sync the project with its memory database.**

Enter the interactive cli (in project directory)

```
activator
```

**if you have some issues with dependencies or if the sbt cache is corrupted, clean and try again**
```
activator clean
```