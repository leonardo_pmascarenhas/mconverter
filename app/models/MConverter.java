package models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Id;

import org.apache.commons.io.FileUtils;

import play.data.format.Formats;
import play.db.ebean.Model;
import resources.Utils;


public class MConverter extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	@Formats.DateTime(pattern = "dd/MM/yyyy")
	private Date date;
	private String from;	
	private String to;	
	private Double value;
	private String result;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void quotation() throws Exception {
		try {
			BigDecimal bDec = currencyQuotation(this.getFrom(), this.getTo(), this.getValue(), Utils.dateToStrPattern(this.getDate()));
			this.setResult(bDec.toString());
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/*
	 * Core function
	 */
	private static final String Q_TYPE_A = "A";
	private static final String Q_TYPE_B = "B";
	public static BigDecimal currencyQuotation(String from, String to, Number value, String quotation) throws Exception {

		// Rule: RESTR_03!
		if (value.doubleValue() < 0) {
			throw new Exception("Invalid value.");
		}

		Date qDate = Utils.strDateValidation(quotation);

		// List of quotations to find ours from and to parameters
		final List<Quotation> quotations;
		quotations = MConverter.getQuotationsByDate(qDate);

		// Get quotations from the loaded values

		Quotation qFrom = findQuotationByCurrencyName(from, quotations);
		Quotation qTo = findQuotationByCurrencyName(to, quotations);

		// Rule: RESTR_02!
		if (qFrom == null || qTo == null) {
			throw new Exception("'From' and/or 'To' parameter(s) invalid(s)");
		}

		MathContext mathContext = new MathContext(2, RoundingMode.HALF_UP);

		//GET BRL Value from the first currency
		BigDecimal valueFrom = new BigDecimal(value.toString());
		BigDecimal buyTaxFrom = qFrom.getBuyTax();
		BigDecimal amountFrom = valueFrom.divide(buyTaxFrom, mathContext);

		//Calc the secound currency with the BRL Value
		BigDecimal valueTo = amountFrom;
		BigDecimal buyTaxTo = qTo.getBuyTax();
		BigDecimal amountTo = valueTo.multiply(buyTaxTo);
		
		return amountTo;
	}

	/*
	 * getValidQuotationDate
	 * Get the valid quotation date
	 * Rule: RESTR_04_1
	 * @Param: Date quotationDate
	 */
	private static Date getValidQuotationDate(Date quotationDate) {
		Calendar qDate = Calendar.getInstance();
		qDate.setTime(quotationDate);
		int day = qDate.get(Calendar.DAY_OF_WEEK);

		switch (day) {
		case Calendar.SATURDAY:
			qDate.add(Calendar.DAY_OF_MONTH, -1);
			return qDate.getTime();

		case Calendar.SUNDAY:
			qDate.add(Calendar.DAY_OF_MONTH, -2);
			return qDate.getTime();

		default:
			return qDate.getTime();
		}
	}

	/*
	 * findQuotationByCurrencyName
	 * Find a quotation in a list by his currencyName
	 * @param String currencyName
	 * @param Lit<Quotation> quotations
	 */
	public static Quotation findQuotationByCurrencyName(String currencyName, List<Quotation> quotations) {
		for (Quotation quotation: quotations) {
			if (quotation.getCurrencyName().equals(currencyName)) {
				return quotation;
			}
		}

		return null;
	}

	/*
	 * getQuotationsByDate
	 * get a list of quotations by a quotation date
	 * @param: Date quotationDate
	 */
	@SuppressWarnings("rawtypes")
	private static List<Quotation> getQuotationsByDate(Date quotationDate) throws Exception {
		//Calendar qDate = Calendar.getInstance();
		Date qDate = getValidQuotationDate(quotationDate);

		String fileName = Utils.dateToStrFileNameFormat(qDate);
		String link = Utils.QUOTATION_LINK + fileName + Utils.QUOTATION_EXTENSION;

		URL url = new URL(link);
		File downloadedFile = new File(link);
		//File downloadedFile = new File("/home/leo/dev/moneyConv/app/20150119.csv"); // Local example

		FileUtils.copyURLToFile(url, downloadedFile);

		//Rule: RESTR_04_2
		if (!Utils.fileExists(link)) {
			System.out.println("[Error] File not available. '" + link +"'");
			throw new Exception("The file requested is not available.");
		}

		BufferedReader br = new BufferedReader(new FileReader(downloadedFile));
		String line;

		String currencyName, typeValue, buyTax, sellTax, date, parityBuy, paritySell;

		List<Quotation> quotations = new ArrayList<Quotation>();
		while((line = br.readLine()) != null){
			String[] b = line.split(";");

			date = b[0];
			typeValue = b[2];
			currencyName = b[3];
			buyTax = b[4];
			sellTax = b[5];
			parityBuy = b[6];
			paritySell = b[7];

			//			System.out.println(line);
			//			System.out.println("date: " + date);
			//			System.out.println("typeValue: " + typeValue);
			//			System.out.println("currencyName: " + currencyName);
			//			System.out.println("buyTax: " + buyTax);
			//			System.out.println("sellTax: " + sellTax);
			//			System.out.println("parityBuy: " + parityBuy);
			//			System.out.println("paritySell: " + paritySell);

			Quotation quotation = new Quotation(date, typeValue, currencyName, buyTax, sellTax, parityBuy, paritySell);
			quotations.add(quotation);
		}
		br.close();

		return quotations;
	}

}

