package models;

import java.math.BigDecimal;
import java.util.Date;

import resources.Utils;


public class Quotation {

	private Date date;
	private String typeValue;
	private String currencyName;
	private BigDecimal buyTax;
	private BigDecimal sellTax;
	private BigDecimal parityBuy;
	private BigDecimal paritySell;

	public Quotation(String date, String typeValue, String currencyName, String buyTax, String sellTax, String parityBuy, String paritySell) throws Exception {
		try {
			this.date = Utils.strDateValidation(date);
			this.currencyName = currencyName;
			this.typeValue = typeValue;
			this.buyTax = Utils.strToBigDecimal(buyTax);
			this.sellTax = Utils.strToBigDecimal(sellTax);
			this.parityBuy = Utils.strToBigDecimal(parityBuy);
			this.paritySell = Utils.strToBigDecimal(paritySell);
		} catch(Exception e) {
			System.out.println(e);
			throw new Exception("Error trying to create a quotation object");
		}
	}

	public String getCurrencyName() {
		return currencyName;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getTypeValue() {
		return typeValue;
	}
	public void setTypeValue(String typeValue) {
		this.typeValue = typeValue;
	}
	public BigDecimal getBuyTax() {
		return buyTax;
	}
	public BigDecimal getSellTax() {
		return sellTax;
	}
	public BigDecimal getParityBuy() {
		return parityBuy;
	}
	public BigDecimal getParitySell() {
		return paritySell;
	}
	
}
