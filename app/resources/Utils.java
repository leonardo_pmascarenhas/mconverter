package resources;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
	
	public static final String DATE_PATTERN = "dd/MM/yyyy";
	public static final String VALUE_PATTERN = "#,##0.0#";	

	public static final String DATE_FILENAME_PATTERN = "yyyyMMdd";
	public static final String QUOTATION_LINK = "http://www4.bcb.gov.br/Download/fechamento/";
	public static final String QUOTATION_EXTENSION = ".csv";

	
	/*
	 * dateValidation
	 * Validates the string date informed
	 * @param String sDate
	 */
	public static Date strDateValidation(String sDate) throws Exception {
		Date result = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(Utils.DATE_PATTERN);
		sdf.setLenient(false);
		
		try {
			result = sdf.parse(sDate);
		} catch (Exception e) {
			throw new Exception("[Error] Trying to convert a date.");
		}

		return result;
	}
	
	/*
	 * dateToStrFileNameFormat
	 * Convert the informed date into a string (result)
	 * This result is used to create the filename that it will be downloaded
	 * @param Date date
	 */
	public static String dateToStrFileNameFormat(Date date) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat(Utils.DATE_FILENAME_PATTERN);
		
		String result;
		try {
			result = formatter.format(date);
		} catch (Exception e) {
			throw new Exception("[Error] Trying to convert a date.");
		}
		
		return result;
	}

	/*
	 * dateToStrPattern
	 * Convert the informed date into a string (DATE_PATTERN)
	 * @param Date date
	 */
	public static String dateToStrPattern(Date date) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
		
		String result;
		try {
			result = formatter.format(date);
		} catch (Exception e) {
			throw new Exception("[Error] Trying to convert a date.");
		}
		
		return result;
	}

	/*
	 * strToBigDecimal
	 * Convert a value informed to BigDecimal
	 * @Param String strValue
	 */
	public static BigDecimal strToBigDecimal(String strValue) throws Exception {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		
//		symbols.setGroupingSeparator(',');
		symbols.setDecimalSeparator(',');
		
		String pattern = Utils.VALUE_PATTERN;
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		decimalFormat.setParseBigDecimal(true);

		// Parse the string
		BigDecimal bigDecimal = (BigDecimal) decimalFormat.parse(strValue);
		
		return bigDecimal;
	}
	
	/*
	 * fileExists
	 * make a request to know if the file really exists
	 * @param: String url
	 */
	public static boolean fileExists(String url){
		try {
			HttpURLConnection.setFollowRedirects(false);
			//HttpURLConnection.setInstanceFollowRedirects(false)
			HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
			con.setRequestMethod("HEAD");
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
