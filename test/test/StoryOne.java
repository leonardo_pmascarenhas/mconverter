package test;

import static org.fest.assertions.Assertions.assertThat;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.callAction;
import static play.test.Helpers.fakeApplication;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.inMemoryDatabase;
import static play.test.Helpers.running;
import static play.test.Helpers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import play.mvc.Result;

public class StoryOne {

//	@Test
	public void tryLogin() { //not working
		running(fakeApplication(inMemoryDatabase()), new Runnable() {
			@Override
			public void run() {
				// LOGIN
				final Map<String, String> data = new HashMap<String, String>();
				data.put("email", "leonardo.pmascarenhas@gmail.com");
				data.put("password", "321321");

				Result result = callAction(
						controllers.routes.ref.Application.doLogin(),
						fakeRequest().withFormUrlEncodedBody(data));

				//	            System.out.println(result);
				//	            assertThat(status(result)).isEqualTo(OK); 

				// RECOVER COOKIE FROM LOGIN RESULT
				//	            final Cookie playSession = play.test.Helpers.cookie("PLAY_SESSION",
				//	                                                                result);

				// LIST SOMETHING (using cookie of the login result)
				//	            result = callAction(controllers.routes.ref.Application.index(), 
				//	                                fakeRequest().withCookies(playSession));
				/* 
				 * WAS RECEIVING 'SEE_OTHER' (303) 
				 * BEFORE RECOVERING PLAY_SESSION COOKIE (BECAUSE NOT LOGGED IN).
				 *
				 * NOW, EXPECTED 'OK'
				 */ 
				//	            assertThat(status(result)).isEqualTo(OK); 
				//	            assertThat(contentAsString(result)).contains(
				//	                    "Something found");
			}
		});
	}	

	@Test
	public void UserShouldVoteJustOnce() {

		running(fakeApplication(inMemoryDatabase()), new Runnable() {
			@Override
			public void run() {
				// INDEX
				Result result;
				
				result = callAction(
						controllers.routes.ref.Application.index(),
						fakeRequest());
				assertThat(status(result)).isEqualTo(OK);
				
				

				final Map<String, String> data = new HashMap<String, String>();
				data.put("name", "Bicao do Zé");
				data.put("address", "Av. Bento Goncalves, 1000");

				result = callAction(
				controllers.routes.ref.Restaurant.save(),
				fakeRequest().withFormUrlEncodedBody(data));
			}
		});
	}
}

